requirejs.config({
    baseUrl: 'js/app',
    paths: {
        app: "../app",
        jquery: '../lib/jquery.min',
        TweenMax: '../lib/TweenMax.min'
    }
});

requirejs(["app/main"]);