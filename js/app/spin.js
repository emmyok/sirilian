define(['jquery', "animations","symbols","TweenMax"],function ($, animations,symbols,TweenMax) {

    $("#spinBttn").click(function () {

       if(animations.isAbleToSpin()){

           animations.disableSpin();
           animations.animateLogoDragons()
           symbols.animateSymbols()
       }

    })

})