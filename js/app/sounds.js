define(['jquery'],function ($) {

    function downloadSounds() {
        $.getJSON("data.json",function (data) {
            var music = data.gameSong
            var audio = new Audio(music)
            audio.loop = true;
            audio.volume = 0.2;
            audio.play();
        })
    }

    return {
        downloadSounds: downloadSounds
    }
})