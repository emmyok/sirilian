define(['jquery', 'TweenMax', 'animations'], function ($, TweenMax, animations) {

    function randomNumber() {
        var randomNumber = Math.floor(Math.random() * 10) + 1;
        return randomNumber;
    }

    function addSymbols() {
        var numbers = [randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber(),randomNumber()]
        $('.symbolsContainers1').append('<div class="symbol winningSymbol winningSymbol1 symbol' + numbers[0]+ '"></div>');
        $('.symbolsContainers1').append('<div class="symbol winningSymbol winningSymbol2 symbol' + numbers[1]+ '"></div>');
        $('.symbolsContainers1').append('<div class="symbol winningSymbol winningSymbol3 symbol' + numbers[2]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol winningSymbol4 symbol' + numbers[3]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol winningSymbol5 symbol' + numbers[4]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol winningSymbol6 symbol' + numbers[5]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol winningSymbol7 symbol' + numbers[6]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol winningSymbol8 symbol' + numbers[7]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol winningSymbol9 symbol' + numbers[8]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol winningSymbol10 symbol' + numbers[9]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol winningSymbol11 symbol' + numbers[10]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol winningSymbol12 symbol' + numbers[11]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol winningSymbol13 symbol' + numbers[12]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol winningSymbol14 symbol' + numbers[13]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol winningSymbol15 symbol' + numbers[14]+ '"></div>');

        for (var x = 4; x <= 296; x++) {
            $('.symbolsContainers1').append('<div class="symbol symbol' + randomNumber()+ '"></div>');
            $('.symbolsContainers2').append('<div class="symbol symbol' + randomNumber()+ '"></div>');
            $('.symbolsContainers3').append('<div class="symbol symbol' + randomNumber()+ '"></div>');
            $('.symbolsContainers4').append('<div class="symbol symbol' + randomNumber()+ '"></div>');
            $('.symbolsContainers5').append('<div class="symbol symbol' + randomNumber()+ '"></div>');
        }

        $('.symbolsContainers1').append('<div class="symbol winningSymbol1 symbol' + numbers[0]+ '"></div>');
        $('.symbolsContainers1').append('<div class="symbol winningSymbol2 symbol' + numbers[1]+ '"></div>');
        $('.symbolsContainers1').append('<div class="symbol winningSymbol3 symbol' + numbers[2]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol4 symbol' + numbers[3]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol5 symbol' + numbers[4]+ '"></div>');
        $('.symbolsContainers2').append('<div class="symbol winningSymbol6 symbol' + numbers[5]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol7 symbol' + numbers[6]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol8 symbol' + numbers[7]+ '"></div>');
        $('.symbolsContainers3').append('<div class="symbol winningSymbol9 symbol' + numbers[8]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol10 symbol' + numbers[9]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol11 symbol' + numbers[10]+ '"></div>');
        $('.symbolsContainers4').append('<div class="symbol winningSymbol12 symbol' + numbers[11]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol13 symbol' + numbers[12]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol14 symbol' + numbers[13]+ '"></div>');
        $('.symbolsContainers5').append('<div class="symbol winningSymbol15 symbol' + numbers[14]+ '"></div>');
    }

    function setWinningSymbols() {
        for(var x=1; x<=15; x++){
            $('.winningSymbol'+x).attr({
                class: 'symbol winningSymbol'+x+' symbol'+randomNumber()
            })

            $(".winningSymbol"+x).first().addClass("winningSymbol")
        }
    }

    function animateSymbols() {
        var tl = new TimelineMax();
        var speed = 6;

        tl.to('.symbolsContainers1', speed, {
            ease: Power4.easeInOut,
            y: -39072
        });
        TweenLite.to('.symbolsContainers2', speed, {
            delay: .1,
            ease: Power4.easeInOut,
            y: -39072
        });
        TweenLite.to('.symbolsContainers3', speed, {
            delay: .2,
            ease: Power4.easeInOut,
            y: -39072
        });
        TweenLite.to('.symbolsContainers4', speed, {
            delay: .3,
            ease: Power4.easeInOut,
            y: -39072
        });
        TweenLite.to('.symbolsContainers5', speed, {
            delay: .4,
            ease: Power4.easeInOut,
            y: -39072,
            onComplete: finishReelAnimation
        });

        setTimeout(function(){
            setWinningSymbols()
        }, 4000)
    }

    function finishReelAnimation() {
        TweenLite.set('.symbolsContainers', {clearProps:"all"});
        animations.animateSymbols()
    }

    return {
        addSymbols: addSymbols,
        animateSymbols: animateSymbols
    }
})