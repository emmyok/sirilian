define(['jquery', 'TweenMax','bonus'], function ($, TweenMax,bonus) {
    var frameWidth = 132;
    var abletoSpin=true;

    function animateLogoDragons() {
        var currentFrame = 0;

        var interval = setInterval(function () {
            currentFrame++

            if (currentFrame > 12) {
                clearInterval(interval)
            }

            $('.dragonOnTheAnimation').css({
                'backgroundPosition': '-' + (currentFrame * 80) + 'px 0px'
            })
        }, 100)
    }

    function animateChains() {
        TweenMax.staggerTo('.dragonWood div', 4, {y: 369, onComplete: finishChainsAnimation}, 0.5)
    }

    function finishChainsAnimation() {
        TweenMax.staggerTo('.dragonWood div', 4, {y: 0, onComplete: finishChainsAnimation}, 0.5)
    }

    function animateDragon() {
        var numCols = 46;
        var symbolNumber = 8;
        animateSymbol(symbolNumber, numCols)
    }

    function animateWine() {
        var numCols = 61;
        var symbolNumber = 10;
        animateSymbol(symbolNumber, numCols)
    }

    function animateMonster() {
        var numCols = 17;
        var symbolNumber = 4;
        animateSymbol(symbolNumber, numCols)
    }
    function animateLamp() {
        var numCols = 18;
        var symbolNumber = 5;
        animateSymbol(symbolNumber, numCols)
    }

    function animateBook() {
        var numCols = 24;
        var symbolNumber = 6;
        animateSymbol(symbolNumber, numCols)
    }

    function animateEgg() {
        var numCols = 28;
        var symbolNumber = 7;
        animateSymbol(symbolNumber, numCols)
    }
    function animateHouse() {
        var numCols = 76;
        var symbolNumber = 9;
        animateSymbol(symbolNumber, numCols)
    }




    function animateSymbol(symbol, numCols) {
        var steppedEase = new SteppedEase(numCols - 1);
        var tl = new TimelineMax({repeat: 0});
        var animSpeed = 2

        tl.add(TweenMax.to('.symbol'+symbol, animSpeed, {
            backgroundPosition: '-' + (frameWidth * (numCols - 1)) + 'px 0px',
            ease: steppedEase,
            onComplete: function() {
                finishSymbolAnimation(symbol)
            }
        }));
        tl.play()
    }

    function finishSymbolAnimation(symbol) {
        TweenLite.set('.symbol'+symbol, {clearProps:"all"});
        abletoSpin=true;

    }

    function animateSymbols() {

        animateDragon()
        animateWine()
        animateMonster()
        animateLamp()
        animateBook()
        animateEgg()
        animateHouse()
        setTimeout(function(){
            bonus.openBonusMode();
        },2000)
    }

    function isAbleToSpin() {
        return abletoSpin;
    }

    function disableSpin() {
        abletoSpin = false;
    }

    return {
        animateLogoDragons: animateLogoDragons,
        animateChains: animateChains,
        animateDragon: animateDragon,
        animateWine: animateWine,
        animateSymbols: animateSymbols,
        isAbleToSpin: isAbleToSpin,
        disableSpin: disableSpin
    }
})